﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ParkingAppServer.Interfaces;
using ParkingAppServer.Models;

namespace ParkingAppServer.Controllers.Services
{
    public class ParkingService : IParkingService
    {
        private Parking parking = Parking.GetInstance();

        public float GetBalance()
        {
            return parking.Balance;
        }

        public float GetProfit()
        {
            return parking.Transactions.Sum(s => s.WithdrawnFunds);
        }

        public (int, int) GetPlaceStatus()
        {
            (int free, int busy) placeStatus = parking.GetPlaceStatus();
            return placeStatus;
        }
        public string GetAllAvailableVehicle()
        {
            return JsonConvert.SerializeObject(GlobalSettings.PARKINGR_RATES.Select(s => new { Number = s.Number, Name = s.Type.Name }));
        }

        public List<Transaction> GetTransactions()
        {
            return parking.Transactions;
        }

        public List<Transaction> GetTransactionsHistory()
        {
            return parking.GetTransactionsLog();
        }

        public List<ParkingPlace> GetAllVehicleOnParking()
        {
            return parking.ParkingPlaces.Where(s => s.Vehicle != null).ToList();
        }

        public VehicleBaseModel AddVehicleInParking(int vehicleType)
        {

            var selectedRateEntry = GlobalSettings.PARKINGR_RATES?.FirstOrDefault(s => s.Number == vehicleType);

            if (selectedRateEntry != null && selectedRateEntry.Value.Type != null)
            {
                Type selectedVehicleType = selectedRateEntry.Value.Type;
                VehicleBaseModel vehicleBaseModel = (VehicleBaseModel)Activator.CreateInstance(selectedVehicleType);
                vehicleBaseModel.VehicleName = selectedVehicleType.Name;

                VehicleBaseModel addedVehicleBaseModel = parking.AddVehicle(vehicleBaseModel);
                return addedVehicleBaseModel;
            }
            return null;
        }

        public VehicleBaseModel DeleteVehicleFromParking(Guid vehicleBaseModel)
        {
            VehicleBaseModel deletedVehicleBaseModel = parking.RemoveVehicle(vehicleBaseModel);
            return deletedVehicleBaseModel;
        }

        public VehicleBaseModel TopUpVehicleBalance(VehicleBaseModel vehicle, float funds)
        {
            VehicleBaseModel topUpVehicleBaseModel = parking.TopUpVehicleBalance(vehicle, funds);
            return topUpVehicleBaseModel;
        }
    }
}
