﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ParkingAppServer.Interfaces;
using ParkingAppServer.Models;

namespace ParkingAppServer.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private IParkingService parkingService;

        public ParkingController(IParkingService service)
        {
            parkingService = service;
        }

        [HttpGet("availableVehicle")]
        public ActionResult<string> GetAvaliableVehicle()
        {         
            return Ok(parkingService.GetAllAvailableVehicle());
        }

        [HttpGet("balance")]
        public ActionResult<float> GetBalance()
        {
            float balance = parkingService.GetBalance();
            return Ok(balance);
        }

        [HttpGet("profit")]
        public ActionResult<float> GetProfit()
        {
            float profit = parkingService.GetProfit();
            return Ok(profit);
        }

        [HttpGet("placeStatus")]
        public ActionResult GetPlaceStatus()
        {
            (int free, int busy) placeStatus = parkingService.GetPlaceStatus();
            JObject jObject = new JObject { ["Free"] = placeStatus.free, ["Busy"] = placeStatus.busy };
            return Ok(JsonConvert.SerializeObject(jObject));
        }

        [HttpGet("transactions")]
        public ActionResult<List<Transaction>> GetTransaction()
        {
            return Ok(parkingService.GetTransactions());
        }

        [HttpGet("transactionsHistory")]
        public ActionResult<List<Transaction>> GetTransactionHistory()
        {
            return Ok(parkingService.GetTransactionsHistory());
        }

        [HttpGet("vehicleOnParking")]
        public ActionResult<List<VehicleBaseModel>> GetAllVehicleOnParking()
        {
            return Ok(parkingService.GetAllVehicleOnParking());
        }

        [HttpPut("addVehicle/{vehicleType}")]
        public ActionResult<VehicleBaseModel> AddVehicle(int vehicleType)
        {
            return Ok(parkingService.AddVehicleInParking(vehicleType));
        }

        [HttpPost("topUpVehicle/{funds}")]
        public ActionResult<VehicleBaseModel> TopUpVehicle([FromBody] VehicleBaseModel vehicle, float funds)
        {
            return Ok(parkingService.TopUpVehicleBalance(vehicle, funds));
        }

        [HttpDelete("deleteVehicle/{vehicleGuid}")]
        public ActionResult<VehicleBaseModel> DeleteVehicle(Guid vehicleGuid)
        {
            return Ok(parkingService.DeleteVehicleFromParking(vehicleGuid));
        }
    }
}
