﻿using ParkingAppServer.Models.VehicleModels;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace ParkingAppServer
{
    public static class GlobalSettings
    {
        public const float PARKING_BALANCE = 0F;
        public const int PARKING_MAX_CAPACITY = 10;
        public const float PARKING_PAYMENT_TIME = 5F;
        public const float PARKING_PENALTY_COEF = 2.5F;

        public static List<(byte Number, Type Type, float Rate)> PARKINGR_RATES = new List<(byte number, Type type, float rate)>()
        {
            (1,typeof(Car), 2f),
            (2,typeof(Truck), 5f),
            (3,typeof(Bus), 3.5f),
            (4,typeof(Motorcycle), 1f)
        };

        public const int PARKING_LOGGER_INTERVAL = 60;
        public const int PARKING_CLAENUP_INTERVAL = 1;
        public const int PARKING_TRANSACTION_TIME_CAPACITY = 60;

        public const string PATH_LOG_FILE = "Transactions.log";
        public const char LOG_SEPARATOR_CHAR = ';';
    }
}
