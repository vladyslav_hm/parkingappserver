﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ParkingAppServer.Models;

namespace ParkingAppServer.Interfaces
{
   public interface IParkingService
    {
        float GetBalance();
        float GetProfit();
        (int, int) GetPlaceStatus();
        string GetAllAvailableVehicle();
        List<Transaction> GetTransactions();
        List<Transaction> GetTransactionsHistory();
        List<ParkingPlace> GetAllVehicleOnParking();
        VehicleBaseModel AddVehicleInParking(int vehicleType);
        VehicleBaseModel DeleteVehicleFromParking(Guid vehicleId);
        VehicleBaseModel TopUpVehicleBalance(VehicleBaseModel vehicle, float funds);           
    }
}
