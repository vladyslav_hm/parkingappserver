﻿using System;
using System.Collections.Generic;
using System.Text;
using ParkingAppServer.Models;
using ParkingAppServer.Interfaces;

namespace ParkingAppServer.Models.VehicleModels
{
    internal class Bus : VehicleBaseModel, IVehicle
    {      
        public void SaySomething()
        {
            Console.WriteLine("Im bus, I can drive with many passengers");
        }
    }
}
