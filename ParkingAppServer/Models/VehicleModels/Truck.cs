﻿using System;
using System.Collections.Generic;
using System.Text;
using ParkingAppServer.Interfaces;

namespace ParkingAppServer.Models.VehicleModels
{
    internal class Truck : VehicleBaseModel, IVehicle
    {     
        public void SaySomething()
        {
            Console.WriteLine("Im truck, I can freight transportation");
        }
    }
}
