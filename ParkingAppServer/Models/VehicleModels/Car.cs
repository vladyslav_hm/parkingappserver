﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using ParkingAppServer.Interfaces;

namespace ParkingAppServer.Models.VehicleModels
{
    internal class Car : VehicleBaseModel, IVehicle
    {    
        public void SaySomething()
        {
            Console.WriteLine("Im car, I can drive with passenger");
        }
    }
}
