﻿using ParkingAppServer.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingAppServer.Models.VehicleModels
{
    internal class Motorcycle : VehicleBaseModel, IVehicle
    {      
        public void SaySomething()
        {
            Console.WriteLine("Im motorcycle, I can make backFlip");
        }
    }
}
