﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ParkingAppServer.Models
{
    public  class JsonModel
    {
        public int Status { get; set; }
        public string ActionName { get; set; }
        public List<object> Args { get; set; }
    }
}
